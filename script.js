// I - ogólne

var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");

var coordinates = document.getElementById("coordinates")
var firstElement = document.getElementById("first")
var secondElement = document.getElementById("second")
var common = document.getElementById("common")
var euclid = document.getElementById("euclid")
var manhattan = document.getElementById("manhattan")
var vector = document.getElementById("vector")
var chooseType =  document.getElementById("chooseType")

var type = 1 //1 - kartezjański; 2 - biegunowy
var counter = 0;
var degsInRad = 180 / Math.PI;
var first, second;
var xNow, yNow;

context.font = "18px Arial";

// II - funkcje clearPlane, getAngle, fixAngle, showCommon

var clearPlane = function () {

	context.clearRect(0, 0, 500, 500);

	context.fillStyle = "black";

	if (type === 1) {

		context.fillText("y", 260, 30)
		context.fillText("x", 470, 270)

		context.beginPath();
		context.moveTo(250, 500);
		context.lineTo(250, 0);
		context.lineTo(260, 10);
		context.moveTo(250, 0);
		context.lineTo(240, 10);

		context.moveTo(0, 250);
		context.lineTo(500, 250);
		context.lineTo(490, 240);
		context.moveTo(500, 250);
		context.lineTo(490, 260);
		context.stroke();

	} else {

		context.beginPath();
		context.moveTo(250, 250);
		context.lineTo(250, 0);
		context.lineTo(260, 10);
		context.moveTo(250, 0);
		context.lineTo(240, 10);
		context.stroke();

		context.beginPath();
		context.arc(250, 250, 3, 0, Math.PI * 2, false);
		context.fill();

		context.fillText("r", 260, 30)

	}

};

clearPlane();


getAngle = function (x, y) {
	if (x === 0) {
		if (y > 0) {
			return 0;
		} else if (y < 0) {
			return Math.PI;
		} else {
			return "Nieokreślony";
		}
	} else if (y === 0) {
		if (x > 0) {
			return Math.PI / 2;
		} else {
			return Math.PI * 3 / 2
		}
	} else {


 			if (x > 0 && y > 0) { //pierwsza ćwiartka
 				return Math.abs(Math.PI / 2 - Math.atan(y / x) );
 			} else if (x > 0 && y < 0) { //czwarta
 				return Math.PI + Math.atan(x / y);
 			} else if (x < 0 && y < 0) { // trzecia
 				return Math.PI + Math.atan(x / y);
 			} else { //druga
 				return  Math.PI * 2 + Math.atan(x / y);

 			}
 		}
 	};


 	var fixAngle = function (angle, forVector) {
 		if (!forVector) {
 			if (angle === "Nieokreślony") {
 				return "Nieokreślony";
 			} else if (angle === 0) {
 				return 0;
 			} else {
 				return angle.toFixed(2) + ' rad <br /><span class="deg">= ' + (angle * degsInRad).toFixed(2) + " deg</span>";

 			}
 		} else {
 			if (angle === "Nieokreślony") {
 				return "Nieokreślony]";
 			} else if (angle === 0) {
 				return 0 + "]";
 			} else {
 				return angle.toFixed(2) + ' rad <br /><span class="deg">= ' + (angle * degsInRad).toFixed(2) + " deg]</span>";

 			}

 		}
 	};

 	var showCommon = function () {
 		let vectorX = second.x - first.x;
 		let vectorY = second.y - first.y;

 		common.style.display = "block";
 		let euclideanDistance = Math.sqrt(vectorX ** 2 + vectorY ** 2).toFixed(2);
 		let manhattanDistance = Math.abs(vectorX) + Math.abs(vectorY);
 		euclid.innerHTML = euclideanDistance;
 		manhattan.innerHTML = manhattanDistance;

 		if (type === 1) {

 			vector.innerHTML = "[" + vectorX + ", " + vectorY + "]";
 		} else {
 			let vectorR = euclideanDistance;
 			let vectorϕ = getAngle(vectorX, vectorY);
 			vector.innerHTML = "[" + vectorR + ", " + fixAngle(vectorϕ, true);
 		}
 	};



 // III konstruktor Point






 var Point = function (x, y) {
 	this.x = x - 250;
 	this.y = 250 - y;

 	this.altX = x;
 	this.altY = y;
 }

 Point.prototype.draw =  function () {
 	context.fillStyle = "blue";
 	context.beginPath();
 	context.arc(this.altX, this.altY, 3, 0, Math.PI * 2, false);
 	context.fill();
 };

 Point.prototype.write = function (element) {
 	if (type === 1) {
 		element.innerHTML = "X: " + this.x + "<br />Y: " + this.y;
 	} else {
 		let angle = getAngle(this.x, this.y);
 		let fixedAngle = fixAngle(angle, false);
 		let distance = Math.sqrt(this.x ** 2 + this.y ** 2).toFixed(2)
 		element.innerHTML = "r: " + distance + "<br />ϕ: " + fixedAngle;
 	}

 }



// IV - obsługa zdarzeń

chooseType.addEventListener("change", function () {
	type = parseInt(chooseType.value);
	clearPlane();
	first.draw();
	second.draw();
	if (counter > 0) {
		first.write(firstElement);
		if (counter === 2) {
			second.write(secondElement);
			showCommon();
		}
	}
}, true);


canvas.addEventListener("mousemove", function (e) {
	xNow = e.offsetX - 250;
	yNow = 250-e.offsetY;
	if (type === 1) {
		coordinates.innerHTML = "X: " + xNow + "<br />Y: " + yNow;
	} else {
		let r = Math.sqrt(xNow ** 2 + yNow ** 2)
		let angle = getAngle(xNow, yNow);
		coordinates.innerHTML = "r: " + r.toFixed(2) + "<br />ϕ: " + fixAngle(angle, false);
	}
}, false);

canvas.addEventListener("click", function (e) {
	counter++;
	if (counter === 3) {
		counter = 1;
	}

	if (counter === 1) {
		clearPlane();
		secondElement.innerHTML = "";
		second = null;
		common.style.display = "none";

		first = new Point(e.offsetX, e.offsetY);
		first.draw();
		first.write(firstElement);

	}

	if (counter === 2) {		
		second = new Point(e.offsetX, e.offsetY);
		second.draw();
		second.write(secondElement);

		showCommon();


	}

}, false);