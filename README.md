Program edukacyjny. Mamy płaszczyznę, za pomocą kliknięć możemy na niej umieścić do 2 punktów. Program podaje 
 - współrzędne punktów
 - współrzędne wektora między tymi punktami
 - odległość między punktami w metrykach Euklidesa i Manhattan
 
Użytkownik może wybrać między współrzędnymi kartezjańskimi a biegunowymi.
Program napisany pod komputery z myszką; ekrany dotykowe nie spiszą się tu najlepiej.